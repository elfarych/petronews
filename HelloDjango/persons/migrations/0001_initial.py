# Generated by Django 3.2.6 on 2021-08-21 19:23

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255, verbose_name='ФИО')),
                ('position', models.CharField(max_length=255, verbose_name='Должность')),
                ('company', models.CharField(max_length=255, verbose_name='Компания')),
                ('image', models.ImageField(blank=True, null=True, upload_to=None, verbose_name='Изображение')),
                ('body', ckeditor_uploader.fields.RichTextUploadingField(verbose_name='Био')),
                ('slug', models.SlugField(help_text='маленькими буквами, без пробелов и спецсимволов', unique=True, verbose_name='Slug')),
                ('order', models.PositiveSmallIntegerField(verbose_name='Порядковые номер')),
            ],
            options={
                'verbose_name': 'Персона',
                'verbose_name_plural': 'Персоны',
                'ordering': ('order',),
            },
        ),
    ]
